<?php
namespace HMR;

use pocketmine\network\protocol\AddEntityPacket;
use pocketmine\network\protocol\SetTimePacket;
use pocketmine\network\protocol\TextPacket;
use pocketmine\network\protocol\AddPlayerPacket;
use pocketmine\entity\Entity;
use pocketmine\nbt\tag\Byte;
use pocketmine\nbt\tag\Compound;
use pocketmine\nbt\tag\Double;
use pocketmine\nbt\tag\Enum;
use pocketmine\nbt\tag\Float;
use pocketmine\nbt\tag\Int;
use pocketmine\math\AxisAlignedBB;
use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\utils\TextFormat;
use pocketmine\event\player\PlayerDeathEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\Player;
use pocketmine\Server;
use pocketmine\math\Vector3;
use pocketmine\level\sound\GhastSound;
use pocketmine\level\sound\BatSound;
class Main extends PluginBase implements Listener{
    

    public function onEnable() {
$this->getServer()->getPluginManager()->registerEvents($this,$this);
        $this->getLogger()->info(TextFormat::BLUE . "HealthMessageReturn Enabled!");
    }

    public function onDeath(PlayerDeathEvent $event) {
        $cause = $event->getEntity()->getLastDamageCause();
        if($cause instanceof EntityDamageByEntityEvent) {
            $player = $event->getEntity();
            $killer = $cause->getDamager();
$p = $event->getEntity();
            if($killer instanceof Player){
            	$fizz = new GhastSound($killer);
            	$bat = new BatSound($player);

	    	$player->sendMessage(TextFormat::RED.$killer->getName() . TextFormat::GOLD." Killed you with " .TextFormat::LIGHT_PURPLE.$killer->getHealth().TextFormat::GOLD." hearts left and while using ".TextFormat::BLUE.$killer->getInventory()->getItemInHand()->getName().TextFormat::GOLD."!");
  	       // $player->getLevel()->addSound($bat);
	   // 	$killer->sendTip(TextFormat::GREEN."You Killed ".TextFormat::LIGHT_PURPLE.$player->getName());
//$killer->setHealth($killer->getMaxHealth());
	    	$killer->getLevel()->addSound($fizz);

		}
            }
        }

}
